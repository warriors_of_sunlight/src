/*
fourLDRservocontrol.c
*/

#include "simpletools.h"
#include "servo.h"

void updown();
void leftright();

int lightUp, lightDown, lightLeft, lightRight, UDdiff, LRdiff;

int main()
{
  cog_run(updown,128);
  cog_run(leftright,128);
}

void updown()
{
  while(1)                                    // Endless loop
  {
    high(2);
    pause(1);
    lightUp = 2*rc_time(2, 1);

    high(0);
    pause(1);
    lightDown = rc_time(0, 1);
    
    UDdiff = 200*lightUp / (lightUp + lightDown) - 100;
    
    int a;                                    // variable for angle of up/down
      
//Up and Down servo direction controls

    if (UDdiff>= 30 && a > 10)            // Will not let the servo rotate past
    {                                         // 180 degress and damage itself. Slowly
      a = a - 10;                             // moves the horn upwards by .1 degree
      servo_angle(12, a);                     // increments at a time so long as the 
      pause(10);                              // buttonup is pressed. Servo will stop
      servo_stop();                           // and hold at this position if no button 
     }                                        // is pressed.
     
     
    else if (UDdiff<= -30 && a < 1810)       // Will not let the servo rotate to a
    {                                         // value less than 0 degrees and ruin
      a = a + 10;                             // its gears. When down button is pressed
      servo_angle(12, a);                     // the horn rotates down by .1 degree at 
      pause(10);                              // a time. It will stay at this postition
      servo_stop();                           // until another button is pressed.
    }   
  }
}       

void leftright()
{
  while(1)
  {
    high(5);
    pause(1);
    lightLeft = rc_time(5, 1);

    high(8);
    pause(1);
    lightRight = rc_time(8, 1);
    
    LRdiff = 200*lightRight / (lightRight + lightLeft) - 100;
    
    int b;                                    // variable for angle of left/right
     
//Left and Right direction controls
  
   if (LRdiff <= -30 && b < 1810)           // Will not let the servo rotate past
   {                                          // 180 degress and damage itself. Slowly
      b = b + 10;                             // moves the horn to the left by .1 degree
      servo_angle(17, b);                     // increments at a time so long as the 
      pause(10);                              // button left is pressed. Servo will stop
      servo_stop();                           // and hold at this position if no button
   }                                          // is pressed.
     
    else if (LRdiff >= 30 && b > 10)      // Will not let the servo rotate to a value
    {                                         // less than 0 degrees and grind its gears.
      b = b - 10;                             // When the right button is pressed, the 
      servo_angle(17, b);                     // horn will rotate right in .1 degree
      pause(10);                              // increments so long as the button is held.
      servo_stop();                           // Holds position if no press.
    }           
  }                       
}
