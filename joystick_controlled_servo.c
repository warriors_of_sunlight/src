#include "adcDCpropab.h"                            // Include adcDCpropab
#include "simpletools.h"                            // Include simpletools
#include "servo.h"                                  // Include servo


int main()                                          // Main function
{
  pause(1000);                                      // Wait 1 s for Terminal app
  adc_init(21, 20, 19, 18);                         // CS=21, SCL=20, DO=19, DI=18

  float lrV, udV;                                   // Voltage variables
  int ud, lr;                                       
  
 

  while(1)                                          // Loop repeats indefinitely
  {
    udV = adc_volts(2);                             // Check A/D 2                
    lrV = adc_volts(3);                             // Check A/D 3
    
    ud = 10+(170*udV)/5;                            // Set variable for up/down motion
    lr = 10+(170*lrV)/5;                            // Set variable for left/right motion
    
    servo_angle(17,lr*10);                          // Set servo position for left/right
    servo_angle(12,ud*10);                          // Set servo position for up/down 
  
    putChar(HOME);                                  // Cursor -> top-left "home"
    print("Up/Down = %d %c\n", ud, CLREOL);         // Display voltage
    print("Left/Right = %d %c\n", lr, CLREOL);      // Display voltage

    pause(100);                                     // Wait 1/10 s
  }  
}

