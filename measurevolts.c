/*
  Measure Volts.c

  Make voltmeter-style measurements with the Propeller Activity Board.
*/

#include "simpletools.h"                      // Include simpletools
#include "adcDCpropab.h"                      // Include adcDCpropab

int main()                                    // main function
{
  adc_init(21, 20, 19, 18);                   // CS=21, SCL=20, DO=19, DI=18

  float v2, v3;                               // Voltage variables

  while(1)                                    // Loop repeats indefinitely
  {              
    v3 = adc_volts(3);                        // Check A/D 3
    
    putChar(HOME);                            // Cursor -> top-left "home"
    print("%f\n",v3);                         // Display volts

    pause(100);                               // Wait 1/10 s
  }  
}