/*
finalsunseeker.c
*/

#include "simpletools.h"
#include "servo.h"
#include "adcDCpropab.h"

void updown();
void leftright();
void joystick();

int lightUp, lightDown, lightLeft, lightRight, UDdiff, LRdiff;

int main()
{
  cog_run(updown,128);
  cog_run(leftright,128);
  cog_run(joystick,128);
}

void updown()
{
 
  while(1)                                    // Endless loop
  {
    int buttonjoy = input(15);
    if (buttonjoy == 0)
    {
      high(0);
      pause(1);
      lightUp = rc_time(0, 1);

      high(2);
      pause(1);
      lightDown = rc_time(2, 1);
    
      UDdiff = 200*lightUp / (lightUp + lightDown) - 100;
    
      int a;                                    // variable for angle of up/down
        
  //Up and Down servo direction controls
  
      if (UDdiff>= 30 && a > 10)                // Will not let the servo rotate less
      {                                         // than 0 degrees and damage itself. If
        a = a - 10;                             // the difference between LDR up and down
        servo_angle(12, a);                     // is greater than 30, the horn will move
        pause(10);                              // the solar panel down until the difference
        servo_stop();                           // between the two LDRs is less than 30.
       }                                        // Position will hold until light changes.
       
       
      else if (UDdiff<= -30 && a < 1810)       // Will not let the servo rotate to a
      {                                         // value greater than 180 degrees and ruin
        a = a + 10;                             // its gears. When UDdiff is less than -30
        servo_angle(12, a);                     // the horn rotates up by .1 degree at 
        pause(10);                              // a time. It will stay at this postition
        servo_stop();                           // until the light changes.
      }   
    }          
  }  
}
     

void leftright()
{
  while(1)
  {
    int buttonjoy = input(15);
    if (buttonjoy == 0)
    {
      high(8);
      pause(1);
      lightLeft = rc_time(8, 1);
  
      high(5);
      pause(1);
      lightRight = 1.5*rc_time(5, 1);           // Proportional constant because this LDR  
                                                // outputs a smaller value than all other LDRs
      LRdiff = 200*lightRight / (lightRight + lightLeft) - 100;
      
      int b;                                    // variable for angle of left/right
        
    //Left and Right direction controls
      
      if (LRdiff <= -30 && b < 1810)             // Will not let the servo rotate past
      {                                          // 180 degress and damage itself. Slowly
         b = b + 10;                             // moves the horn to the left by .1 degree
         servo_angle(17, b);                     // increments at a time so long as the 
         pause(10);                              // difference between LDR left and right is
         servo_stop();                           // less than -30.Servo will hold this position 
      }                                          // unless the light changes.
         
       else if (LRdiff >= 30 && b > 10)          // Will not let the servo rotate to a value
       {                                         // less than 0 degrees and grind its gears.
         b = b - 10;                             // When the difference betweeen LDR left and right 
         servo_angle(17, b);                     // is greater than 30, the horn will rotate right 
         pause(10);                              // in 0.1 degree increments until te difference is
         servo_stop();                           // less than 30 degrees and holds this position
       }                                         // until the light changes.
     }   
   }                      
}


void joystick()
{
  while(1)
  { 
  int buttonjoy = input(15)  ;
    if (buttonjoy == 1)
    {
      pause(100);
      adc_init(21, 20, 19, 18);
    
      float lrV, udV;
      int ud, lr;
      
      udV = adc_volts(2);
      lrV = adc_volts(3);
      
      ud = 10+(170*udV)/5;
      lr = 10+(170*lrV)/5;
      
      servo_angle(17, lr*10);
      servo_angle(12, ud*10);
      
      pause(100);
     } 
   }  
 }